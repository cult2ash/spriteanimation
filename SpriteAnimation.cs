using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[ExecuteInEditMode]
public class SpriteAnimation : MonoBehaviour {
    [System.Serializable]
    public struct KeyFrame
    {
        public Sprite sprite;
        public float time;
    }
    
    public bool IsPlayOnAwake;
    public bool IsLoop;
    
    public float Length => frames.Count>0?
        frames[frames.Count-1].time:
        0f;
    
    [SerializeField]
    List<KeyFrame> frames=new List<KeyFrame>();
   
    public void AddKeyFrame(Sprite sp,float time)
    {
        frames.Add( new KeyFrame
        {
            sprite = sp,
            time = time
        });
    } 
    
    public void Start()
    {
        if(IsPlayOnAwake) Play();
    }
    
    public void Clear()
    {
        frames.Clear();
    }
    public void Stop()
    {
        StopCoroutine(nameof(PlayAnimation));
    }
    
    public void Play()
    {
        StartCoroutine(nameof(PlayAnimation));
    }
    
    IEnumerator PlayAnimation()
    {
        
        int current;
        float time;
        SpriteRenderer renderer=GetComponent<SpriteRenderer>();
        do{
            if(frames.Count>0) {
                current=0;
                time=0f;
                renderer.sprite=frames[current].sprite;
                while(current<frames.Count-1) {
                    time+=Time.smoothDeltaTime;
                    if(time>=frames[current+1].time)
                    {
                        current++;
                        renderer.sprite=frames[current].sprite;
                    }
                    yield return null;
                }
            } else break;
        } while(IsLoop);
       
    }
    
}
