using System.Collections;
using System.IO;
using System.Linq;
using System.Text;
using I2;
using UnityEditor;
using UnityEngine;
using YamlDotNet.RepresentationModel;

[CustomEditor(typeof(SpriteAnimation),true)]
public class SpriteAnimationInscpecter : Editor {
    [MenuItem("GameObject/2D Object/Sprite Animation", false, 0)]
    static void AddObject()
    {
        GameObject s = Selection.activeGameObject;
		
        if (s != null)
        {
            GameObject go = new GameObject("SpriteAnimation", typeof(SpriteAnimation));
            go.transform.SetParent(s.transform, false);
        }
    }
    
    public override void OnInspectorGUI()
    {
        var sa = (SpriteAnimation)target;
        Rect myRect = GUILayoutUtility.GetRect(0,20,GUILayout.ExpandWidth(true));
        GUI.Box(myRect,"요기로 애니메이션 드래그");
            
            if (myRect.Contains(Event.current.mousePosition))
            {
                if (Event.current.type == EventType.DragUpdated)
                {
                    DragAndDrop.visualMode = DragAndDropVisualMode.Copy;
                
                    Event.current.Use ();
                }   
                else if (Event.current.type == EventType.DragPerform)
                {
                    sa.Clear();
                    for(int i = 0; i<DragAndDrop.paths.Length;i++)
                    {
                        byte[] b;
                        using (FileStream fs = new FileStream(Application.dataPath + "/../" + DragAndDrop.paths[i], FileMode.Open, FileAccess.Read))
                        {
                             
                            var yaml=new YamlStream();
                            yaml.Load(new StreamReader(fs));
                            
                            var mapping =
                                ((YamlMappingNode)yaml.Documents[0].RootNode).Children["AnimationClip"]["m_PPtrCurves"][0];
                           
                            var squence=(YamlSequenceNode)(((YamlMappingNode)mapping).Children["curve"]);
                            foreach(var ttt in squence.Children)
                            {
                                Debug.Log(ttt["time"]);
                                Debug.Log(ttt["value"]["fileID"]);
                                Object[] sprites=AssetDatabase.LoadAllAssetsAtPath(AssetDatabase.GUIDToAssetPath(ttt["value"]["guid"].ToString()));
                                for(int x=0;x<sprites.Length;x++)
                                {
                                    string guid;
                                    long fileid;
                                    AssetDatabase.TryGetGUIDAndLocalFileIdentifier(sprites[x],out guid,out fileid);
                                    if(ttt["value"]["fileID"].ToString().Equals(fileid.ToString()))
                                    {
                                        sa.AddKeyFrame(sprites[x] as Sprite, float.Parse(ttt["time"].ToString()));
                                       break;
                                        
                                    }
                                }
                               
                            }
                          
                            fs.Close();
                            
                        }
                    }
                    Event.current.Use ();
                }
            
            } 
     
        sa.IsPlayOnAwake=GUILayout.Toggle(sa.IsPlayOnAwake,"PlayOnAwake");
        sa.IsLoop=GUILayout.Toggle(sa.IsLoop,"Loop");
        GUILayout.Label("Animation Length:"+sa.Length);
        if(GUILayout.Button("Play Animation")) {
            sa.Play();
        }
       // GUILayout.EndArea();
    }
    
 
}
